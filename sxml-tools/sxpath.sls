#!r6rs
;; Copyright 2009 Derick Eddington.  My MIT-style license is in the file named
;; LICENSE from the original collection this file is distributed with.

(library (wak sxml-tools sxpath (2008 06 27))
  (export
    sxpath
    if-sxpath
    if-car-sxpath
    car-sxpath
    sxml:id-alist)
  (import
    (rnrs)
    (wak private include)
    (srfi :2 and-let*)
    (wak sxml-tools sxml-tools)
    (wak sxml-tools sxpathlib)
    (wak sxml-tools sxpath-ext)
    (wak sxml-tools txpath)
    (wak ssax private output))

  (include-file ("wak" "sxml-tools" "upstream") "sxpath.scm")
)
