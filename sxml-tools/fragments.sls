#!r6rs
;; Copyright 2009 Derick Eddington.  My MIT-style license is in the file named
;; LICENSE from the original collection this file is distributed with.

(library (wak sxml-tools fragments (2008 06 27))
  (export
    sxml:display-fragments
    SRV:send-reply
    sxml:display-feed
    sxml:clean-fragments
    sxml:clean-feed
    sxml:shtml->http
    sxml:fragments->http)
  (import
    (rnrs)
    (wak private include)
    (wak sxml-tools sxml-tools))

  (include-file ("wak" "sxml-tools" "upstream") "fragments.scm")
)
