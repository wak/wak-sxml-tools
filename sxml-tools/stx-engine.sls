#!r6rs
;; Copyright 2009 Derick Eddington.  My MIT-style license is in the file named
;; LICENSE from the original collection this file is distributed with.

(library (wak sxml-tools stx-engine (2008 06 27))
  (export
    stx:version
    sxml:stylesheet
    stx:apply-templates
    stx:find-template)
  (import
    (rnrs)
    (wak private include)
    (wak sxml-tools sxml-tools)
    (wak sxml-tools sxpathlib)
    (wak ssax private output)
    (wak ssax private error))

  (define stx:error (make-errorer "(wak sxml-tools stx-engine)"))

  (include-file ("wak" "sxml-tools" "upstream") "stx-engine.scm")
)
