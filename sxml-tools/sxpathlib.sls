#!r6rs
;; Copyright 2009 Derick Eddington.  My MIT-style license is in the file named
;; LICENSE from the original collection this file is distributed with.

(library (wak sxml-tools sxpathlib (2008 06 27))
  (export
    nodeset?
    as-nodeset
    sxml:element?
    ntype-names??
    ntype??
    ntype-namespace-id??
    sxml:complement
    node-eq?
    node-equal?
    node-pos
    sxml:filter
    take-until
    take-after
    map-union
    node-reverse
    node-trace
    select-kids
    node-self
    node-join
    node-reduce
    node-or
    node-closure
    sxml:node?
    sxml:attr-list
    sxml:attribute
    sxml:child
    sxml:parent
    node-parent
    sxml:child-nodes
    sxml:child-elements)
  (import
    (rnrs)
    (wak private include)
    (rename (except (srfi :13 strings) string-copy string->list string-titlecase
                    string-upcase string-downcase string-hash string-for-each)
            (string-index-right string-rindex))
    (wak ssax private misc)
    (wak ssax private output)
    (rename (only (rnrs io simple) write)
            (write pp)))

  (include-file ("wak" "sxml-tools" "upstream") "sxpathlib.scm")
)
