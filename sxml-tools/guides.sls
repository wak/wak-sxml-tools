#!r6rs
;; Copyright 2009 Derick Eddington.  My MIT-style license is in the file named
;; LICENSE from the original collection this file is distributed with.

(library (wak sxml-tools guides (2008 06 27))
  (export
    dgs:version
    dgs:fold
    dgs:find
    add-lp
    sxml-guide-flat
    sxml-guide
    xml-guide-flat)
  (import
    (rnrs)
    (rnrs mutable-pairs)
    (wak private include)
    (wak ssax parsing)
    (wak ssax private output))

  (include-file ("wak" "sxml-tools" "upstream") "guides.scm")
)
