#!r6rs
;; Copyright 2009 Derick Eddington.  My MIT-style license is in the file named
;; LICENSE from the original collection this file is distributed with.

(library (wak sxml-tools xpath-ast (2008 06 27))
  (export
    txp:ast-operation-helper
    txp:ast-params
    txp:ast-res
    txp:ast-api-helper
    txp:xpath->ast
    txp:xpointer->ast
    txp:expr->ast
    txp:sxpath->ast
    txp:step?
    txp:step-axis
    txp:step-node-test
    txp:step-preds
    txp:construct-step)
  (import
    (rnrs)
    (wak private include)
    (srfi :2 and-let*)
    (wak sxml-tools xpath-parser)
    (wak ssax private output))
                
  (include-file ("wak" "sxml-tools" "upstream") "xpath-ast.scm")
)
