;; Copyright 2010 Andreas Rottmann.  Distributed under an MIT-style
;; license, which can be found in the file named LICENSE from the
;; original collection this file is distributed with.

(package (wak-sxml-tools (0) (2008 06 27) (1))
  (depends (srfi)
           (wak-common)
           (wak-ssax))
  
  (synopsis "tools for manipulating SXML")
  (description
   "This package provides the following tools:"
   " - SXPath, an XPath implementation for SXML"
   " - SXML serialization to XML and HTML"
   " - XLink parsing and processing")
  
  (libraries
   ("sxml-tools" -> ("wak" "sxml-tools"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
